
import 'dart:io';

void main() {

	int row = 4;
	
	for(int i = 1; i <= row; i++) {

		for(int j = 2*(row-i); j >= 1; j--) {
	
			stdout.write(" ");
		}
		
		int num = row;
		for(int j = 1; j <= i; j++) {

			stdout.write("$num ");
			num--;
		}
		stdout.writeln();
	}
}
