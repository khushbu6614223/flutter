//WAP to print a table of 5 in reverse order

void main() {

	int i = 50;

	while(i >= 1) {

		if(i%5 == 0)

			print(i);
		i--;
	}
}
