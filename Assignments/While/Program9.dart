//WAP to print the countdown of days to submit the assignment


void main() {

	int numDays = 7;

	while(numDays >= 0) {

		if(numDays >= 1) 
		
			print("$numDays days remaining");

		else  

			print("$numDays days Assignment is Overdue");

		numDays--;
	}

}
