//Write a program that takes a number, if the number is even print that number in reverse order, or if the number is odd print that number in reverse order by difference two.



void main() {

	int num = 7;
	if(num%2 == 0) {

		while(num != 0) {

			print("$num ");
			num--;
		}
	} else {
	
		while(num >= 0) {
		
			print(num);
			num = num - 2;		
		}
	}
}
