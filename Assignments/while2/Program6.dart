//WAP to print the sum of all even numbers and the multiplication of odd numbers between 1 to 10


void main() {

	int num = 1;
	int sum = 0;
	int multi = 1;

	while(num <= 10) {

		if(num%2 == 0) {
	
			sum = sum + num;
		}	
		else {
	
			multi = multi * num;
		}
		num++;
	}
	print("Sum of even numbers between 1 to 10 = $sum");
	print("Multiplication of odd numbers between 1 to 10 = $multi");
}
