import 'dart:io';

void main() {

	int? empId;
	String? empName;
	double? empSal;

	print("Enter employee Id");
	empId = int.parse(stdin.readLineSync()!);

	print("Enter employee Name");
	empName = stdin.readLineSync();

	print("Enter employee Salary");
	empSal = double.parse(stdin.readLineSync()!);

	print("Employee Id = $empId");
	print("Employee Name = $empName");
	print("Employee Salary = $empSal");
}
