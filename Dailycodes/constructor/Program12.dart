//3rd way
//Default Parameter


class Company {

	int? empCount;
	String? compName;

	Company(this.empCount ,{this.compName = "Biencaps"});

	void compInfo() {

		print(empCount);
		print(compName);
	}
}

void main() {

	Company obj1 = new Company(100);
	obj1.compInfo();
}
