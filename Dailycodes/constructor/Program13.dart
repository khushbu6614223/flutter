//4th way
//Named Parameter


class Company {

	int? empCount;
	String? compName;

	Company({this.empCount ,this.compName});

	void compInfo() {

		print(empCount);
		print(compName);
	}
}

void main() {

	Company obj1 = new Company();
	obj1.compInfo();

	Company obj2 = new Company(empCount : 100);
	obj2.compInfo();

	Company obj3 = new Company(empCount : 200 , compName : "Biencaps");
	obj3.compInfo();

	Company obj4 = new Company(compName : "Incubator" , empCount : 300);
	obj4.compInfo();
}
