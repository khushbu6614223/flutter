class Demo {

	static int x = 10;
	int y = 20;

	Demo() {

		print("Constructor");
	}
	static void m1() {

		print(x);
	}
	void m2() {

		print(y);
	}
}

void main() {

	Demo obj = new Demo();
	Demo.m1();
	obj.m2();
}
