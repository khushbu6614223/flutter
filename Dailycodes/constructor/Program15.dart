//Parameterized constructor

class Employee {

	int? empId;
	String? empName;

	Employee(this.empId , this.empName) {
	
		print("Parameterized constructor");
	}
}
void main() {
	
	Employee obj = new Employee(100 , "Biencaps");
}
