//Named constructor

class Employee {

	int? empId;
	String? empName;

	Employee() {
	
		print("Default Constructor");
	}

	Employee.constr(int empId , String empName) {
	
		print("Parameterized constructor");
	}
}
	
void main() {
	
	Employee obj1 = new Employee();
	Employee obj2 = new Employee.constr(10, "Kanha");
}
