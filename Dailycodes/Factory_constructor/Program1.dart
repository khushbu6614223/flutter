class Demo {

	Demo._private() {

		print("Inn Constructor");
	}
	factory Demo() {

		print("In factory Constructor");
		return Demo._private();
	}
}
