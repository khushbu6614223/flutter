
//Singelton Design Pattern using static


class Demo {

	static Demo obj = new Demo._private();

	Demo._private() {

		print("In constructor");
	}
	static Demo getInstance() {

		return obj;
	}
}
