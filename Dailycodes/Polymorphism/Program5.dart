

class Parent {

	void career() {

		print("Engineering");
	}
	void marry() {

		print("Deepika Padukone");
	}
}

class Child extends Parent {

	void marry() {

		print("Disha Patni");
	}
	void profession() {

		print("Software Engineering");
	}
}

void main() {

	Parent obj1 = new Parent();
	obj1.career();
	obj1.marry();


	Child obj2 = new Child();
	obj2.career();
	obj2.marry();
	obj2.profession();
}
