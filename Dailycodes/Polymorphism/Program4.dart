

class Parent {

	void career() {

		print("Engineering");
	}
	void marry() {

		print("Deepika Padukone");
	}
}

class Child extends Parent {

	void marry() {

		print("Disha Patni");
	}
}

void main() {

	Child obj = new Child();
	obj.career();
	obj.marry();
}
