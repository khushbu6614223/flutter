//Real time example

abstract class Developer {

	void develop() {

		print("We build s/w");
	}
	void devTypes();
}

class MobileDev extends Developer {

	void devTypes() {

		print("Flutter Developer");
	}
}

class WebDev extends Developer {

	void devTypes() {

		print("Web Developer");
	}
}

void main() {


	Developer obj2 = new MobileDev();
	obj2.develop();
	obj2.devTypes();

	Developer obj3 = new WebDev();
	obj3.develop();
	obj3.devTypes();

	WebDev obj4 = new WebDev();
	obj4.develop();
	obj4.devTypes();

}
