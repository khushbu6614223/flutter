class Parent {

	int x = 10;

	Parent() {

		print("Parent constructor");
		print(this.hashCode);
	}

	void printData() {

		print(x);
	}
}

class Child extends Parent {

	int x = 20;
	
	Child() {

		print("Child constructor");
		print(this.hashCode);
	}

	void dispData() {

		print(x);
		print(super.x);
	}

}

void main() {

	Child obj2 = new Child();
	obj2.printData();
	obj2.dispData();
}
