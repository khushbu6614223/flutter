class Parent {

	Parent() {

		print("Parent constructor");
	}

	call() {

		print("IN call method");
	}
}

class Child extends Parent {

	Child() {

		print("Child constructor");
	}
}

void main() {

	Child obj = new Child();
	obj();

}
