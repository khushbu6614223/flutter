//Multilevel Inheritance


class ICC {

	ICC() {

		print("ICC constructor");
	}
}
class BCCI extends ICC {

	BCCI() {

		print("BCCI Constructor");
	}
}

class IPL extends BCCI {

	IPL() {

		print("IPL Constructir");
	}
}

void main() {

	IPL obj = new IPL();
}
