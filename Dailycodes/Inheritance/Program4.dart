

class Parent {

	int x = 10;
	String str = "name";

	void parentMethod() {

		print(x);
		print(str);
	}
}

class Child extends Parent {

	int y = 20;
	String str1 = "data";

	void childMethod() {

		print(y);
		print(str1);
	}
}

void main() {

	Child obj = new Child();
	print(obj.x);
	print(obj.str);

	obj.parentMethod();

	print(obj.y);
	print(obj.str1);

	obj.childMethod();

}
