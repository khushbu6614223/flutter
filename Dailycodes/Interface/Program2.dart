

abstract class IFC {

	void material() {

		print("Indian Material");
	}
	void taste();
}
class IndianFc implements IFC {

	void material() {

		print("Indian material");
	}
	void taste() {

		print("Indian taste");
	}
}

class EUFC extends IFC {

	void taste() {

		print("Europian taste");
	}
}

void main() {

	IndianFc obj = new IndianFc();
	obj.material();
	obj.taste();
}
