abstract class Demo1 {

	void m1() {

		print("In m1-Interface");
	}
}

abstract class Demo2 {

	void m2() {

		print("In m2-Interface");
	}
}

class Demo implements Demo1,Demo2 {

	void m1() {

		print("In m1");
	}
	void m2() {

		print("In m2");
	}
}
void main() {

	Demo obj = new Demo();
	obj.m1();
	obj.m2();
}
