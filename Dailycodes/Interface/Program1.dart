abstract class Developer {

	int x = 10;

	Developer() {

		print("Dev Constructor");
	}
	void develop() {

		print("we build s/w");
	}
	void devType();
}

class MobileDev implements Developer {

	int x = 10;
	MobileDev() {
	
		print("MobileDev Constructor");
	}
	void develop() {

		print("we build s/w");
	}
	void devType() {

		print("Flutter-dev");
	}
}

void main() {

	Developer obj = new MobileDev();
	obj.develop();
	obj.devType();

}
