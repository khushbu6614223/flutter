

//Named Parameter
//representation - {}

void main() {

	print("Start main");
	fun(name: "Kanha", sal: 29);
	print("ENd main");
}

void fun({String? name, double? sal}) {

	print("In fun");
	print(name);
	print(sal);

}
