//Optional parameter with default values
//representation- []


void main() {

	print("Start main");
	fun("Kanha");
	fun("Kanha", 20.5);
	print("End main");
}

void fun(String name, [double sal = 10.5]) {

	print("In fun");
	print(name);
	print(sal);
}
