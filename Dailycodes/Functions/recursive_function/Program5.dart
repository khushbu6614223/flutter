//print 5 to 1 in reverse order using for loop and then using recyrsion


void fun() {

	print("In fun");
	for(int i = 5; i >= 1; i--) {
	
		print(i);
	}
}

void gun(int x) {

	if(x == 0)

		return ;
	print(x);
	x--;
	gun(x);
}

void main() {

	fun();
	print("End fun");
	gun(5);
}
