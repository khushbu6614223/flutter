//print sum of the number
//I/p : 5
//O/p : 15


int sum = 0;

void fun(int x) {

	if(x == 0) 

		return ;
	sum = sum + x;
	x--;
	fun(x);
}

void main() {

	fun(5);
	print(sum);
}
