class ProgramLang implements Iterator {

	int index = -1;
	var progLang = [];

	ProgramLang(String lang) {

		this.progLang = lang.split(",");
	}
	
	get current {

		if(index >=0 && index <= progLang.length-1) {

			return "${progLang[index]}";
		}
	}

	bool moveNext() {

		if(index < progLang.length-1) {

			index++;

			return true;
		}
		return false;
	}
}

void main() {

	ProgramLang obj = new ProgramLang("CPP,Java,Python,Dart");

	while(obj.moveNext()) {

		print(obj.current);
	}
}
