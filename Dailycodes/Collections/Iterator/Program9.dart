void main() {

	var players = ["Rohit", "Shubhman", "Virat", "KLRahul", "Shreyas", "Hardik"];

	//contains
	var retVal = players.contains("Shreyas");
	print(retVal);

	//elementAt
	var retVal1 = players.elementAt(5);
	print(retVal1);

	//every
	var retVal2 = players.every((player) => player[0] == "Z");
	var retVal3 = players.every((player) => players.length >4);
	print(retVal2);
	print(retVal3);

	//firstWhere
	var retVal4 = players.firstWhere((player) => player[0] == "S");
	print(retVal4);

	//lastWhere
	var retVal5 = players.lastWhere((player) => player[0] == "S");
	print(retVal5);

	//fold
	var initVal = "";
	var retVal6 = players.fold(initVal,(prevVal,player) => prevVal + player);
	print(retVal6);

	//followedBy
	var newPlayer = ["Jadeja" , "Bumrah"];
	var retVal7 = players.followedBy(newPlayer);
	print(retVal7);

	//forEach
	players.forEach((element) => print(element));

	//join
	var retVal8 = players.join("->");
	print(retVal8);

	//map
	var retVal9 = players.map((player) => player + "Ind");
	print(retVal9);

	//reduce
	var retVal10 = players.reduce((value,player) => value + player);
	print(retVal10);

	//singleWhere
	var retVal11 = players.singleWhere((player) => player.length == 6);
	print(retVal11);

	//skip
	var retVal12 = players.skip(4);
	print(retVal12);
	
	//skipWhile
	var retVal13 = players.skipWhile((element) => element.length == 5);
	print(retVal13);

	//take
	var retVal14 = players.take(5);
	print(retVal14);
	
	//takeWhile
	var retVal15 = players.takeWhile((element) => element[0] == "R");
	print(retVal15);

	//toList
	var retValue1 = players.toList();
	print(retValue1);

	//toSet
	var retValue2 = players.toSet();
	print(retValue2);

	//where
	var retValue3 = players.where((player) => player[0] == "S");
	print(retValue3);
}
