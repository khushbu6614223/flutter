

import 'dart:collection';

void main() {

	var progLang = Queue();

	//add();
	progLang.add("CPP");
	progLang.add("Java");
	progLang.add("Dart");

	print(progLang);

	//addFirst();
	progLang.addFirst("Python");
	print(progLang);

	//addLast();
	progLang.addLast("Java");
	print(progLang);

	
        var lang = ["ReactJs" , "SpringBoot" , "Flutter"];

        //addAll();
        progLang.addAll(lang);
        print(progLang);
 
	//elementAt();
	print(progLang.elementAt(3));
	
	//contains();
	print(progLang.contains("Dart"));

	print(progLang.toSet());
	print(progLang.toList());
	print(progLang.toString());

	//remove();
        progLang.remove("Dart");
        print(progLang);

        //removeFirst();
        progLang.removeFirst();
        print(progLang);

        //removeLast();
        progLang.removeLast();
        print(progLang);

        //removeWhere();
        progLang.removeWhere((lang) => lang.startsWith("J"));
        print(progLang);

	//clear
	progLang.clear();
	print(progLang);
	
}
