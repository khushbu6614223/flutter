//from constructor

import 'dart:collection';

void main() {

	var compData = Queue();

	compData.add("Microsoft");
	compData.add("Amazon");
	compData.add("Google");
	
	print(compData);

	var compData1 = Queue.from(compData);
	print(compData1);

	compData1.add("Veritas");
	print(compData1);
}
