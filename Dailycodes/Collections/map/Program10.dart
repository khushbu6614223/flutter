//SplayTreeMap


import 'dart:collection';

void main() {

	var player = SplayTreeMap();

	//1
	player[18] = "Virat";
	print(player);

	//2
	player.addAll({45:"Rohit"});
	print(player);

	//3
	player.addEntries({7 : "MSD" , 1 : "KLRahul" }.entries);
	print(player);

	player.update(18, (value) => "Virat Kohli");
	print(player);
}
