//UnmodifiableMapView

import 'dart:collection';

void main() {

	var player = SplayTreeMap();

	player.addAll({18 : "virat" , 45 : "Rohit" , 7 : "MSD" , 1 : "KLRahul"});
	print(player);

	var constPlayer = UnmodifiableMapView(player);
	print(constPlayer);

}
