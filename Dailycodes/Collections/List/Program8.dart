//empty constructor

void main() {

	List player1 = List.empty();         //fixed length
	List player2 = List.empty(growable : true);   //growable length
	
	print(player2);

	player2.add("Virat");
	player2.add("Rohit");

	print(player2);

	player2[0] = "MSD";
	print(player2);
}
