//unmodifiable constructor

void main() {

	List player1 = ["virat","Rohit","KL"];

	List player2 = List.unmodifiable(player1);

	print(player1);
	print(player2);

	player1[1] = "Rohit Sharma";
	print(player1);
	print(player2);

}
