//Methods of List

void main() {

	var progLang = List.empty(growable : true);

	progLang.add("CPP");
	progLang.add("Java");
	progLang.add("Dart");
	progLang.add("Java");

	print(progLang);
	print(progLang[2]);

	//elementAt();
	print(progLang.elementAt(3));

	//getRange();
	print(progLang.getRange(0,3));

	//indexOf();
	print(progLang.indexOf("Java"));

	//lastIndexOf();
	print(progLang.lastIndexOf("Java"));

	//indexWhere();
	print(progLang.indexWhere((lang) => lang.startsWith("D")));

	print(progLang);
	var lang = ["ReactJs" , "SpringBoot" , "Flutter"];


	//addAll();
	progLang.addAll(lang);
	print(progLang);

	//insert();
	progLang.insert(3,"Python");
	print(progLang);

	//insertAll();
	progLang.insertAll(3,["Go" , "Swift"]);
	print(progLang);

	//replaceRange();
	progLang.replaceRange(3,7,{"Dart","Python","Java"});
	print(progLang);

	//remove();
	progLang.remove("Dart");
	print(progLang);

	//removeAt();
	progLang.removeAt(2);
	print(progLang);

	//removeLast();
	progLang.removeLast();
	print(progLang);

	//removeRange();
	progLang.removeRange(2,4);
	print(progLang);
	
	//removeWhere();
	progLang.removeWhere((lang) => lang.startsWith("J"));
	print(progLang);

	//clear();
	progLang.clear();
	print(progLang);


}
