//unmodifiable constructor

void main() {

	Set player1 = {"Virat","Rohit","KL"};

	Set player2 = Set.unmodifiable(player1);

	print(player1);
	print(player2);

	player1.add("MSD");
	print(player1);
	print(player2);
}	
