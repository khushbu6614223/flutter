abstract mixin class Demo {

	void fun1() {

		print("In fun1");
	}
	void fun2();
}
class Asach {

	void ashi() {

		print("In ashi method");
	}
}

class Child extends Asach with Demo {

	void fun2() {

		print("In fun2");
	}
}
void main() {

	Demo obj = new Child();
	obj.fun1();
	obj.fun2();

}
