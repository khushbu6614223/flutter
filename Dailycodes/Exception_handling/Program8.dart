
import 'dart:io';

void main() {

	int? x;
	try {

		x = int.parse(stdin.readLineSync()!);
		print(x);
	} on FormatException {

		print("Wrong Input");
	}  catch(Exception) {

		print('Generic');
	}
	print("End Code");
	
}
