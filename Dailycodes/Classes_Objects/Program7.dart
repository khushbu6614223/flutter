//Instance variables


class Employee {

	int empId = 10;
	String Name = "Ashish";
	double sal = 1.35;

	void empInfo() {

		print(empId);
		print(Name);
		print(sal);
	}
}

void main() {

	Employee obj1 = new Employee();
	obj1.empInfo();
	
	print("     ");

	Employee obj2 = new Employee();
	obj2.empInfo();
}
