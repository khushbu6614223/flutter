
import 'dart:io';

class Employee {

	int? empId = 15;
	String? name = "Shashi";
	double? sal = 1.02;

	void empInfo() {

		print("Employee id = $empId");
		print("Employee name = $name");
		print("Employee salary = $sal");
	}
}
void main() {

	Employee obj = new Employee();
	obj.empInfo();
	

	print(" ");

	print("Enter Id: ");
	obj.empId = int.parse(stdin.readLineSync()!);

	print("Enter employee name: ");
	obj.name = stdin.readLineSync();

	print("Enter salary: ");
	obj.sal = double.parse(stdin.readLineSync()!);

	obj.empInfo();

}
